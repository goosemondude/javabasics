package com.swcguild.mvcdemo;

import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping({"/hello"})
public class HelloController {
        
    public HelloController() {
    }
    
    @RequestMapping(value="/sayhi", method=RequestMethod.GET)
    public String sayHi(Map<String, Object> model) {
        model.put("message", "Your face is great!" );
        return "hello";
    }
    
    @RequestMapping(value="/sayhi", method=RequestMethod.POST)
    public String sayWhat(Map<String, Object> model) {
        model.put("message", "Your face is great!" );
        return "hello";
    }
}
