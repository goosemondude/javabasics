/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.rsvp;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "RSVP", urlPatterns = {"/TallyInvitation"})
public class RSVP extends HttpServlet {

    ArrayList<String> peopleRSVPd = new ArrayList<>();
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // So, we've hopefully gotten here via our submit buttonf rom the index page form
        String rsvp = request.getParameter("rsvp");
        String whosComing = request.getParameter("name");

        String msg = "";

        if ("da".equals(rsvp) && whosComing != null && !whosComing.isEmpty()) {
            msg = "SO EXCITED BRUH. I am sew ex heighted!";
            peopleRSVPd.add(whosComing);
        } else if ("nyet".equals(rsvp)) {
            msg = "JESH DarNIT> You are not so nice.";
        } else if (whosComing == null || whosComing.isEmpty()) {
            msg = "BROOOOOO. You can't just like NOT put a name in BROOOOooooo.";
        } else {//if its weird or null do this
            msg = "How did you get here?!";
            request.setAttribute("badInput", true);
        }

        request.setAttribute("msgInABottle", msg);
        RequestDispatcher forwarder = request.getRequestDispatcher("response.jsp");
        forwarder.forward(request, response);
    }
    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.setAttribute("peopleList", peopleRSVPd);
        request.setAttribute("numPeople", peopleRSVPd.size());
        RequestDispatcher forwarder = request.getRequestDispatcher("rsvplist.jsp");
        forwarder.forward(request, response);
        
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
