<%-- 
    Document   : response
    Created on : Jul 7, 2016, 2:26:55 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Here are the people who have RSVP'd so far!</title>
    </head>
    <body>
        <h1>TONS of people are interested, but these are the folks who have said yes...</h1>
        <c:out value="${numPeople}" default="Nada" /> people have RSVP'd. <br/>
        <c:forEach items="${peopleList}" var="person"> ${person},</c:forEach>
    </body>
</html>
