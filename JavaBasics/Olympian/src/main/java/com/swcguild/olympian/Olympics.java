/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.olympian;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class Olympics {

    public static void main(String[] args) {

        // Old way of instantiation
        RussianTrackAthlete lenny = new RussianTrackAthlete();
        System.out.println("Lenny competed in this event: " + lenny.competeInEvent());

        ApplicationContext ctxFactory = new ClassPathXmlApplicationContext("applicationContext.xml");
        RussianTrackAthlete benniCarla = (RussianTrackAthlete) ctxFactory.getBean("benni-carla");
        // benniCarla = ctxFactory.getBean("benni-carla", RussianTrackAthlete.class);
        System.out.println("Benni-Carla competed in THIS event: " + benniCarla.competeInEvent());

        System.out.println("Getting sally outta the factory and making her compete...");
        System.out.println(ctxFactory.getBean("sally", Olympian.class).competeInEvent());
        
        
        Olympian pavel = ctxFactory.getBean("pavel", Olympian.class);
        pavel.competeInEvent();
        
        Olympian hermione = ctxFactory.getBean("hermione", Olympian.class);
        hermione.competeInEvent();
        
    }
}
