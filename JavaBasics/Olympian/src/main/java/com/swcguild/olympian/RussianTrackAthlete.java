/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.olympian;

/**
 *
 * @author apprentice
 */
public class RussianTrackAthlete {

    private Event event;

    public RussianTrackAthlete() {
        event = new RussianTrackEvent();
    }

    public String competeInEvent() {
        return event.compete();
    }

}
