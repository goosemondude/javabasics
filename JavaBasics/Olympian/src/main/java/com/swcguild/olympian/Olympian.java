/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.olympian;

/**
 *
 * @author apprentice
 */
public class Olympian {
    
    Event event;
    String country = "Atlantis";
    
    public String competeInEvent(){
        System.out.println("Now competing for " + country);
        return event.compete();
    }
    
    
    public Olympian (Event event){
        this.event = event;
        
    }
    
    public void setCountry(String country){
        this.country = country;
    }
    
}
