package com.swcguild.olympian;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class OlympicsTest {

    public OlympicsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testRussianTrackStar() {
        RussianTrackAthlete lenny = new RussianTrackAthlete();
        assertEquals(lenny.competeInEvent(), "RussianTrack");
    }       
    
        @Test
        public void testBenniCarlaRussianTrackStar(){
        
       // benniCarla = ctxFactory.getBean("benni-carla", RussianTrackAthlete.class);
        ApplicationContext ctxFactory = new ClassPathXmlApplicationContext("applicationContext.xml");
        RussianTrackAthlete benniCarla = (RussianTrackAthlete) ctxFactory.getBean("benni-carla");
        assertEquals(benniCarla.competeInEvent(), "RussianTrack");
    }

    
}
