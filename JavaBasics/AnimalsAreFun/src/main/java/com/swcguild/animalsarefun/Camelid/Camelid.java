/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.animalsarefun.Camelid;

import com.swcguild.animalsarefun.Animal;
import com.swcguild.animalsarefun.interfaces.Awesome;

/**
 *
 * @author apprentice
 */
public abstract class Camelid extends Animal implements Awesome{
    int amtOfSpit;
    
    abstract public boolean spit();
    
}
