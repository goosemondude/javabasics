/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.javabasics.collections;
import java.util.ArrayList;


/**
 *
 * @author apprentice
 */
public class ListsAndMapsOhMy {
    
    public static void main(String[] args) {
        
        ArrayList<String> starWarsCharacters = 
                new ArrayList<>();
        
        System.out.println("Do I know any SW characters yet? : " 
                + !starWarsCharacters.isEmpty());
        
        
        starWarsCharacters.add("Jabba the Hutt");
        starWarsCharacters.add("Chewbacca");
        starWarsCharacters.add("Han Solo");
        starWarsCharacters.add("Boba Fett");
        starWarsCharacters.add("Luke Skywalker");
        starWarsCharacters.add("Obi Wan");
      
       System.out.println("Do I know any SW characters yet? : " 
                + !starWarsCharacters.isEmpty()); 
       
       for (int i = 0; i < starWarsCharacters.size(); i++) {
           
           System.out.println("This is the " + i + "th one in my list" + starWarsCharacters.get(i));
           
           //This is LIKE starWarsCharacters[i] if it was an array instead
           String character = starWarsCharacters.get(0);
           System.out.println("This is the first one in my list " + character);
           
           
       }
       
       for(String character : starWarsCharacters){
           System.out.println("I really like " + character + ", they are my FAVORITE!");
       }
       // ArrayLosts don't enforce uniqueness, so you can have one item
       // MANY different times in the same group if you want.
       
       starWarsCharacters.add("Chewbacca");
       starWarsCharacters.add("Chewbacca");
       starWarsCharacters.add("Chewbacca");
       starWarsCharacters.add("Chewbacca");
       starWarsCharacters.add("Chewbacca");
       starWarsCharacters.add("Chewbacca");
       
       
       // You can remove items by index in an ArrayList
       starWarsCharacters.remove(0);
       // OR you can remove them by the object itself
       // but it only removes the first one of that object
       starWarsCharacters.remove("Chewbacca");
       
       for (String character : starWarsCharacters) {
           System.out.println(character);
       }
       
        System.out.println("Trying to remove Jabba a second time, when he is not there");
        System.out.println(" returns this as a value : " + starWarsCharacters.remove("Jabba the Hutt"));
    }
}
