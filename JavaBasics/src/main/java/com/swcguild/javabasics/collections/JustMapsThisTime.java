/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.javabasics.collections;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class JustMapsThisTime {
    public static void main(String[] args) {
        
        // So this just sets up the map aka bank
        // It establishes that you will be storing strings as values
        // and using strings as keys
        
        HashMap<String, String> favoriteColors = new HashMap<>();
        
    // You store things in the bank using the put method
    // you have to show up with the key AND the value you are storing
    //favoriteColors.put(key , value)
        favoriteColors.put("John", "Green");
        favoriteColors.put("Katie", "Black");
        favoriteColors.put("Jakob", "Blue");
        favoriteColors.put("Michael", "CHOCOLATE");
        favoriteColors.put("Austyn", "Red");
        
        System.out.println("Michael's favorite color is: " + favoriteColors.get("Michael"));
        
        // Showing up with same key, but a different value
        // does NOT store both instances of the value, it overwrites the old
        favoriteColors.put("Michael", "Blue");
        
        // So now Michael's old color is lost, but the new one is there
        System.out.println("Michael's favorite color is: " + favoriteColors.get("Michael"));
        
        System.out.println("If you try to get something out of the bank "
                + "\nwith a key that hasn't been used to store something . . . "
                + "\nyou will get this as a result: " + favoriteColors.get("Jabba the Hutt"));
        
        // set is a group of nonordered, unique keys/things 
        
        Set<String> peopleNames = favoriteColors.keySet();
        System.out.println("These are the people we know have favorite colors...");
        for ( String name: peopleNames)
            System.out.println(name);
        
        System.out.println("These are the colors that we know people like: ");
        
        for ( String color: favoriteColors.values()){
            
            System.out.println(color);
        }
            HashMap<String, Integer> howManyLikeColors = new HashMap<>();
            // Somaps are often used for collecting as WELL as indexing
            for (String color : favoriteColors.values()) {
                if(howManyLikeColors.containsKey(color)){
                    // increment the number of people who like that color
                    int numLiked = howManyLikeColors.get(color);
                    numLiked++;
                    howManyLikeColors.put(color, numLiked);
                } else {
                    // add a new entry to the map with '1' person who likes that color
                    howManyLikeColors.put(color, 1);
                }
                
            }
         for(Entry<String, Integer> keyValuePair: howManyLikeColors.entrySet())   {
             System.out.println(keyValuePair.getKey() + " is liked by " + keyValuePair.getValue() + " people");
             
         }
        
        
    }
}
