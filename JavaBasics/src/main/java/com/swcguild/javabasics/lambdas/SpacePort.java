/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.javabasics.lambdas;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class SpacePort {

    public static void main(String[] args) {

        List<Spaceship> hangar = new ArrayList<>();

        //String color, String pilotName, String shieldType,
        //  int numFins, int numEngines, boolean hasLifeSupport
        Spaceship shinyShip = new Spaceship("Blue", "Yerrble Beeblebrox", "Bad", 10, 3, false);

        hangar.add(shinyShip);

        Spaceship notAsShinyShip = new Spaceship("Puce", "Jo Jordan", "Great", 0, 1, true);

        hangar.add(notAsShinyShip);

        hangar.add(new Spaceship("Yellow w/ White Stripes", "Meep", "Not so bad", 5, 2, true));
        hangar.add(new Spaceship("Rusty Green", "MrSquiggles", "Average", 40, 3, true));
        hangar.add(new Spaceship("Black w/ Black Stripes", "MrSpacersworth", "Don't need em", 400, 1, false));
        hangar.add(new Spaceship("Platinum", "Silver Fox", "Shady but work sometimes", 27, 4, true));
        hangar.add(new Spaceship("Honey Gold", "Florian", "Stellar", 6, 3, true));
        hangar.add(new Spaceship("Red w/ White Lightning Bolts", "Spearbord", "Average", 13, 12, false));
        hangar.add(new Spaceship("Cobalt", "Merdroar", "Great", 10, 5, true));
        hangar.add(new Spaceship("Stool Brown", "Humbucket", "Seen better", 234, 0, true));
        hangar.add(new Spaceship("Orange Orange", "Fellowsel", "Double Layer", 3, 1, true));
        hangar.add(new Spaceship("Grey w/ white stripes", "Safgar", "Triple Layer", 1, 5, true));

        System.out.println("We can print them out the old school way ...");
        for (Spaceship ship : hangar) {
            System.out.println(ship.getPilotName());
        }

        System.out.println("OR we cna do it with our new fancy method...");
        hangar
                .stream()
                .forEach(ship -> System.out.println(ship.getPilotName()));

        System.out.println("Old school printing out ships pilots whose ships have more than 5 fins...");

        for (Spaceship ship : hangar) {
            if (ship.getNumFins() > 5) {
                System.out.println(ship.getPilotName());
            }
        }

        System.out.println("Fancy way of printing out ships pilots whose ships have more than 5 fins...");

        hangar.stream()
                //notice you can basically steal the conditional from your \
                // if statement in the 'old school' way, above for your filter
                // .filter is predicate
                .filter(sShip -> sShip.getNumFins() > 5)
                // .forEache is consumer
                .forEach(sShip -> System.out.println(sShip.getPilotName()));

        int sumFins = 0;
        for (Spaceship spaceship : hangar) {
            sumFins = spaceship.getNumFins() + sumFins;

        }
        double avgFins = ((double) sumFins) / hangar.size();

        System.out.println("Avg Num of fins in all ships " + avgFins);

        double avgFinsAgain
                = hangar.stream()
                .mapToInt(ship -> ship.getNumFins())
                .average()
                .getAsDouble();
        System.out.println("Avg Num of fins in all ships: " + avgFinsAgain);

        List<Spaceship> shipsWithoutLifeSupport
                = hangar.stream()
                .filter(ship -> !ship.hasLifeSupport())
                .collect(Collectors.toList());

        List<Spaceship> shipsWithLifeSupport
                = hangar.stream()
                .filter(ship -> ship.hasLifeSupport())
                .collect(Collectors.toList());
    }
}
