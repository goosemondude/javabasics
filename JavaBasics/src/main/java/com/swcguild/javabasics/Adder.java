/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.javabasics;

/**
 *
 * @author apprentice
 */
public class Adder {
 
    public static void main(String[] args) {
        
        // declare and initialize the sum
        int sum = 0;
        
        // declare and init operands
        int op1 = 9;
        int op2 = 101;
        
        // assign the sum of the operands to the variable sum
        sum = op1 + op2;
        
        System.out.println(sum);
        
    }
    
}
