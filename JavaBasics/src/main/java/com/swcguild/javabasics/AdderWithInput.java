/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.javabasics;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class AdderWithInput {
    public static void main(String[] args) {
        
        // declare and initialize sum & operand variables
        int sum = 0;
        int op1 = 0;
        int op2 = 0;
        
        // Declare and init Scanner object - the Scanner reads input from the console
        Scanner sc = new Scanner(System.in);
        
        // Because the scanner is reading from the console, and the console is
        // based on string (i.e. text) input, we are going to get input back 
        // via strings, so we'll make a couple of string variables to hold that information
        // before translating it into the proper int type.
        
        String stringOp1 = "";
        String stringOp2 = "";
        
        // Tell the user to input a number...
        System.out.print("Please enter the first number to be added: ");
        
        // Retrieve the input and store it in the first string operand variable
        stringOp1 = sc.nextLine();
        
        // Do that again, but for the second one...
        System.out.print("Please enter the second number to be added: ");
        stringOp2 = sc.nextLine();
        
        // In order to add the values input by the user we have to convert it
        // into number values, so to do that we use the parseInt method in the
        // Integer class to do that.
        op1 = Integer.parseInt(stringOp1);
        op2 = Integer.parseInt(stringOp2);
        
        // Then, we add both numbers together and store it in the sum
        sum = op1 + op2;
        
        // Finally, print it out to the user!!!
        System.out.println("Sum is: " + sum);
        
    }
    
}
