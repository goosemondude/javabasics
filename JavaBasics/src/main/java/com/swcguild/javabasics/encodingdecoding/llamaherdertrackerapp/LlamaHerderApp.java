/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.javabasics.encodingdecoding.llamaherdertrackerapp;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */



public class LlamaHerderApp {
    
 static Scanner sc = new Scanner(System.in);
 
    public static void main(String[] args) {
        
        boolean keepRunning = true;
        
        ArrayList<Llama> llamaHerd = loadLlamaHerdFromFile();
        
        while(keepRunning){
            printMenu();
            int userChoice = Integer.parseInt(sc.nextLine());
            switch(userChoice){
                case 1: //New Llama Entry
                    Llama llama = addNewLlama();
                    llamaHerd.add(llama);
                    break;
                    
                case 2: // List All Llamas
                    break;
                    
                case 3: // Find Llama
                    break;
                    
                case 4: // Save & Exit
                    saveLlamaHerdTofile(llamaHerd);
                    System.out.println("Thank you for using our app!");
                    keepRunning = false;
                    break;
                default: // New Menu Choice
                    System.out.println("I'm sorry, that's not an option. " +
                            "\nPlease choose again.");
                   
                
                    
            }
            
            
            }
        
        
        }
        
        public static Llama addNewLlama(){
            System.out.println("What is the new llama name?");
            String llamaName = sc.nextLine();
            System.out.println("What is the new llama age?");
            double llamaAge = Double.parseDouble(sc.nextLine());
            System.out.println("What is the new llama's wool like?");
            String llamaWool = sc.nextLine();
//            
//            // Lets do this first, with the default constructor
//            Llama aNewLlama = new Llama();
//            aNewLlama.setAge(llamaAge);
//            aNewLlama.setName(llamaName);
//            aNewLlama.setWoolType(llamaWool);

            Llama aNewLlama = new Llama(llamaName, llamaAge, llamaWool);
            
            return aNewLlama;
        }
    
        public static void printMenu(){
            System.out.println("1. Enter Llamas\n2. List All Llamas in Herd\n3. Find Llama Info\n4. Save & Exit"
                    + " Program");
        
    }
        
        public static ArrayList<Llama> loadLlamaHerdFromFile(){
            ArrayList<Llama> llamaHerd = new ArrayList<>();
            try {
                FileReader librarian = new FileReader("llamaHerd.txt");
                BufferedReader reader = new BufferedReader(librarian);
                Scanner prompter = new Scanner(reader);
                
                while (prompter.hasNextLine()){
                    String fileLine = prompter.nextLine();
                    String[] lineTokens = fileLine.split("::");
                    // If there aren't 3 exact tokens, this is not a llama record
                    // And we don't know wha to do with it, so skip to the next line
                    // and hope that THAT one is okay
                    if (lineTokens.length != 3)
                        continue;
                    
                    Llama aNewLlama = new Llama();
                    aNewLlama.setName(lineTokens[0]);
                    aNewLlama.setAge(Double.parseDouble(lineTokens[1]));
                    aNewLlama.setWoolType(lineTokens[2]);
                    llamaHerd.add(aNewLlama);
                }
            
            
            } catch(FileNotFoundException e){
                System.out.println("Your file wasn't there.");
                System.out.println("But don't wory, we'll make one when we exit.");
            }
            
            return llamaHerd;
            
        }
        
        public static void saveLlamaHerdTofile(ArrayList<Llama> llamaHerd){
            try { 
                FileWriter calligrapher = new FileWriter("llamaherd.txt");
                PrintWriter author = new PrintWriter(calligrapher);
                for (Llama squishy : llamaHerd) {
                    String fileLine = "";
                    
                    fileLine = squishy.getName() + "::"
                            + squishy.getAge() + "::"
                            + squishy.getWoolType();
                    
                    author.println(fileLine);
                }
                
                // Make sure that lazy author writes what
                // they said they'd write!
                author.flush();
                author.close();
                
            } catch (IOException e){
                System.out.println("Something exploded.");
                System.out.println(e.getCause());
            }
            
            
            
        }
}
