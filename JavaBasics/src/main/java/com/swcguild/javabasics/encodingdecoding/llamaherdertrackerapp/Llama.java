/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.javabasics.encodingdecoding.llamaherdertrackerapp;

/**
 *
 * @author apprentice
 */
public class Llama {
    private String name;
    private double age;
    private String woolType;
    
public Llama(String name, double age, String woolType) {
        this.name = name;
        this.age = age;
        this.woolType = woolType;
    }
    
    
    public Llama(){}
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAge() {
        return age;
    }

    public void setAge(double age) {
        this.age = age;
    }

    public String getWoolType() {
        return woolType;
    }

    public void setWoolType(String woolType) {
        this.woolType = woolType;
    }

    
    
}
