/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.javabasics.objects;

/**
 *
 * @author apprentice
 */
public class SuperHero {
    
    private String name;
    private String power;
    private String secretIdentity;
    public String lastKnownLocation;
    
    //constructor // alows you to create object
    public SuperHero(String name, String power){
        this.name = name;
        this.power = power;
        
    }
    
    // A super hero will tell anyone who asks, their name.
    // But they don't let anyone change it!
    public String getName(){
        return this.name;
    }
    
    // Super heros will tell other people their power.
    public String getPower(){
        return this.power;
    }
    
    // And they will even let outside things change their power (change existing object)
    public void setPower(String power){
        this.power = power;
    }
    
    // However, while their secret identity can change
    // they aren't going to tell anyone about it.
    public void setSecretIdentity(String newIdentity){
        secretIdentity = newIdentity;
    }
    // secretIdentity = property vs newIdentity = parameter
}
