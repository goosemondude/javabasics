/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.javabasics.objects;

/**
 *
 * @author apprentice
 */
public class ObjectPractice {
    public static void main(String[] args) {
        
        // This is a new construct = constructor plus class name;
        SuperHero wonderWoman = new SuperHero("Wonder Woman", "Justice and Magic");
        SuperHero spiderMan = new SuperHero("Spider Man","Like a Spider, with silk coming out uncomfortable places.");
        SuperHero mystique = new SuperHero("Mystique", "Super awesome shape changer, and also blue.");
        SuperHero theFlash = new SuperHero("The Flash", "Really really fast, and a bit lame.");
        
        System.out.println("Wonder woman will tell you her name: ");
        System.out.println(wonderWoman.getName());
        
        System.out.println("Spiderman's power: " + spiderMan.getPower());
        System.out.println("Unfortunately, Spiderman then got bitten by a mongoose.");
        spiderMan.setPower("The power of a rabid weasel creature.");
        System.out.println("Spiderman's NEW power: " + spiderMan.getPower());
        spiderMan.setPower("The power of sucking.");
        System.out.println("Spiderman's NEW NEW power: " + spiderMan.getPower());
        
        // mystique.setName("Lily Thompson") doesn't work // Can't do this, it's read only!!
        
        mystique.setSecretIdentity("Lily Thompson");
        // mystique.getSecretIdentity() // Does not work, it is write only!
        
        theFlash.lastKnownLocation = "4th and Broadway";
        theFlash.lastKnownLocation = "7th and Main";
        theFlash.lastKnownLocation = "Under the Sea";
        theFlash.lastKnownLocation = "";
        
    }
}
