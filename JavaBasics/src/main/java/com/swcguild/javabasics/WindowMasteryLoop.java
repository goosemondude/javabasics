/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.javabasics;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class WindowMasteryLoop {
    public static void main(String[] args) {
        
        // Declare & initialize our variables
        Scanner sc = new Scanner(System.in);
        double area = 0;
        double perimeter = 0;
        double width, height = 0;
        
        // You can place 'final' before a variable type if it will stay constant AND change variable name
        
        //25.5 for max height
        //18.75 for max width
        //1.0 for min height and min width
        
        final double MAX_WINDOW_HEIGHT = 25.5;
        final double MIN_WINDOW_HEIGHT = 1.0;
        final double MAX_WINDOW_WIDTH = 18.75;
        final double MIN_WINDOW_WIDTH = 1.0;
        
        final double TRIM_PRICE = 2.25;
        final double  GLASS_PRICE = 3.50;
        
        double glassCost = 0;
        double trimCost = 0;
        
        double totalCost = 0;
        
        // Ask user for width
        System.out.print("What is the width of your window? ");
        String widthInput = sc.nextLine();
        width = Double.parseDouble(widthInput);
        if(width > MAX_WINDOW_WIDTH) {
            System.out.println("Sorry, the maximum window width size is " + MAX_WINDOW_WIDTH + ", substituting now.");
            width = MAX_WINDOW_WIDTH;
        } else if(width < MIN_WINDOW_WIDTH) {
            System.out.println("Sorry, the minimum window width size is " + MIN_WINDOW_WIDTH + ", substituting now.");
            width = MIN_WINDOW_WIDTH;
        } 
        
        
        // Ask user for height
        System.out.print("What is the height of your window? ");
        String heightInput = sc.nextLine();
        height = Double.parseDouble(heightInput);
        if(height > MAX_WINDOW_HEIGHT) {
            System.out.println("Sorry, the maximum window height size is " + MAX_WINDOW_HEIGHT + ", substituting now.");
            height = MAX_WINDOW_HEIGHT;
        } else if(height < MIN_WINDOW_HEIGHT) {
            System.out.println("Sorry, the minimum window height size is " + MIN_WINDOW_HEIGHT + ", substituting now.");
            height = MIN_WINDOW_HEIGHT;
        }
        
        // Calculate & display area
        
        area = height * width;
        System.out.println("Window area is: " + area);
        
        // Calculate & display perimiter
        perimeter = 2 * (width + area);
        System.out.println("Window perimiter is: " + perimeter);
        
        // Calculate & display glassCost, trimCost & totalCost
        glassCost = area * GLASS_PRICE;
        trimCost = perimeter * TRIM_PRICE;
        totalCost = glassCost + trimCost;
        
        System.out.println("Window glass costs: $" + glassCost + ", and trim costs: $" + trimCost);
        System.out.println("Total cost of window is: $" + totalCost);
        
        
        
    }
}