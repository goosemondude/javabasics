/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.javabasics.methods.fileio;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ReaderOfThings {
    public static void main(String[] args) throws Exception {
        
        FileReader librarian = new FileReader("MyFile.txt");
        BufferedReader translator = new BufferedReader(librarian);
        Scanner prompter = new Scanner(translator);
        
        while(prompter.hasNextLine()){
            String currentLine = prompter.nextLine();
                System.out.println(currentLine);
        }
        
    }
}
