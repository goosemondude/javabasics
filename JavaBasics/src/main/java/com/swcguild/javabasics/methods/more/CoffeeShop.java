/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.javabasics.methods.more;

/**
 *
 * @author apprentice
 */
public class CoffeeShop {
 
    public static void printMenu(){
        System.out.println("Drip Coffee - $.05");
        System.out.println("Cafe Au Lait - $3.00");
        System.out.println("Choc. Chip Scone - FREEEEEEEeeeeee!!!!");
        
    }
    
    public static String getItemPrice(String itemName){
        
        boolean isDripCoffee = itemName.equalsIgnoreCase("Drip Coffee"); 
        boolean isCafeAuLait = itemName.equalsIgnoreCase("Cafe Au Lait");
        boolean isBestSconeEver = itemName.equalsIgnoreCase("Choc. Chip Scone");
        
        
//        Way #1
//        switch (itemName){
//            case "Drip Coffee":
//                return "$.05";
//            case "Cafe Au Lait":
//                return "$3.00";
//            case "Choc. Chip Scone":
//                return "FreeEEEEEeeEEeeeeeeEeeEEEEEeeeeeeEEE";
//            
//        }
//         return "This is not a valid choice!!";

        //Way #2
//        switch (itemName){
//            case "Drip Coffee":
//                return "$.05";
//            case "Cafe Au Lait":
//                return "$3.00";
//            case "Choc. Chip Scone":
//                return "FreeEEEEEeeEEeeeeeeEeeEEEEEeeeeeeEEE";
//            default:
//                return "This is not a valid choice!!";
//        }
//         
        //Way #3
        String itemPrice = "";
        switch (itemName){
            case "Drip Coffee":
                itemPrice =  "$.05";
                break;
            case "Cafe Au Lait":
                itemPrice = "$3.00";
                break;
            case "Choc. Chip Scone":
                itemPrice = "FreeEEEEEeeEEeeeeeeEeeEEEEEeeeeeeEEE";
                break;
            default:
                itemPrice = "We don't sell that.";
        }
         return itemPrice;
    }
    
}
