/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.javabasics.methods.more;

/**
 *
 * @author apprentice
 */
public class MethodExecution {
    public static void main(String[] args) {
        
        
        Calculator.subtract(6.0, 3.5);
        int returnResult = Calculator.subtract(5,2);
        System.out.println(returnResult);
        
        System.out.println(Calculator.subtract(7.9,1));
        
        System.out.println(Calculator.subtract(Calculator.subtract(1.0, 2),5));
        
        MethodExecution.thisIsMyMethod();
        thisIsMyMethod();
        
        // NON static methods, require you to create an instance of the class
        // that they reside inside.
        
        //Fido belongs to the instance.
        //Static must belong to the class.
        Dog fido = new Dog();
        fido.bark();
        
        //
        
        CoffeeShop.printMenu();
        
        CoffeeShop.getItemPrice("Llama");
        System.out.println(CoffeeShop.getItemPrice("Drip Coffee"));
        
    }
    
    public static void thisIsMyMethod() {
        System.out.println("This is MY method.");
}
}
