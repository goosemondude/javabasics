/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.javabasics.methods;

import com.swcguild.javabasics.controlflow.LoopDeLoops;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */


public class MethodsDemo {

    // this is a main method - IT IS STATIC
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        System.out.print("Give me two numbers: ");
        int a = sc.nextInt();
        int b = sc.nextInt();
        
        int sum = a + b;
        System.out.println("The sum of those numbers is: " + sum);
        System.out.println("The sum of those numbers is: " + addNums(a,b));
        
//        sum = addNums(1, 9);
//        System.out.println("The sum of 1 & 9 is: " + sum);
//        sum = addNums(1, 100);
//        System.out.println("The sum of 1 & 100 is: " + sum);
//        sum = addNums(1, 7484);
//        System.out.println("The sum of 1 & 7484 is: " + sum);
//        sum = addNums(1, 2);
//        System.out.println("The sum of 1 & 2 is: " + sum);
//        sum = addNums(1, -8);
//        System.out.println("The sum of 1 & -8 is: " + sum);

        LoopDeLoops.main(args);
        
    }
    
    
    public static int addNums(int x, int y){
        return x + y;
    }
    
    public static int returnOne(){
        return 1;
    }
    
    public static int rollDie(){
        Random r = new Random();
        int dieRoll = r.nextInt(6) + 1;
        return dieRoll;
    }
    
    public static int rolSidedDie(int dieSides){
        Random r = new Random();
        int dieRoll = r.nextInt(dieSides) + 1;
        return dieRoll;
    }
    
    public static int rollWeightedDie(int dieSides){
        return dieSides;
    }
    
    public static void sayHi(){
        System.out.println("Hello!!!");
    }
    
    public static void greeting(String name){
        System.out.println("Hello" + name + "!!!");
    }
}
