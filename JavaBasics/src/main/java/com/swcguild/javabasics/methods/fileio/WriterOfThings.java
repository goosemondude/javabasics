/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.javabasics.methods.fileio;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;



/**
 *
 * @author apprentice
 */
public class WriterOfThings {
    public static void main(String[] args) throws IOException {

        // PrintWriter writes to the file.
        // FileWriter creates the file - if the file doesn't exist, it will make it.
        // If it DOES exist, it creates a new one over top of it, and the old one is lost.
        
        PrintWriter author = new PrintWriter(new FileWriter("MyFile.txt"));
        // Otherwise you can just write stuff to the file much like System.out.print...
        author.println("Hey, I'm writing things to a file...!");
        author.println("Isn't this great? It's pretty great.");
        author.println("I can even write stuff from a for loop, watch this!!!");
        for (int i = 0; i < 10; i++) {
            author.print(i + " ");
        }
        author.println("Okay, I'm done now.");
        // Writers can be laze, make sure all the stuff it is PLANNING on writing 
        // it actually finishes writing to the file with .flush()
        // Then close it out, so that it isn't accessing the file anymore so someone
        // else can read (or write!) to that location later. 
        author.flush(); 
        author.close();
                
    }
    
}
