/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.javabasics.methods.more;

/**
 *
 * @author apprentice
 */
public class Calculator {
    
    public static int subtract(int num1, int num2){
        int result = num1 - num2;
        return result;
    }
    
    public static double subtract(double num1, double num2) {
        double result = num1 - num2;
        System.out.println(result);
        return result;
    }
    
}
