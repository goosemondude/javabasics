/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.javabasics.controlflow;
import java.util.Scanner;
/**
 *
 * @author apprentice
 */
public class DaysOfTheWeek {
    public static void main(String[] args) {
        
        int day;
        String dayName = "";
        
        // Take user input
        Scanner sc = new Scanner(System.in);
        System.out.print("What day of the week would you like to convert? ");
        day = sc.nextInt();
        
        
        // week starts on Monday
        if (day == 1) {
            dayName = "Monday";
        } else if (day == 2) {
            dayName = "Tuesday";
        } else if (day == 3) {
            dayName = "Wednesday";
        } else if (day == 4) {
            dayName = "Thursday";
        } else if (day == 5) {
            dayName = "Friday";
        } else if (day == 6) {
            dayName = "Saturday";
        } else if (day == 7) {
            dayName = "Sunday";
        } /*else {
            dayName = "not a real day!";
        }*/
            
        if (day < 1 || day > 7) {
            System.out.println("That is not a real day!");
        } else {
            System.out.println("It's " + dayName);
        }
    }
}
