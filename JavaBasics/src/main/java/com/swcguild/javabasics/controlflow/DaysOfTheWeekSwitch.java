/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.javabasics.controlflow;
import java.util.Scanner;
/**
 *
 * @author apprentice
 */
public class DaysOfTheWeekSwitch {
    public static void main(String[] args) {
        
        int day;
        String dayName = "";
        
        // Take user input
        Scanner sc = new Scanner(System.in);
        System.out.print("What day name of the week would you like to convert? ");
        dayName = sc.next();
        
        dayName = dayName.toLowerCase();
        
        // week starts on Monday
        switch (dayName) {
            case "monday":
            case "mon":
            case "m":
                day = 1;
                break;
            case "tuesday":
            case "tues":
            case "t":
                day = 2;
                break;
            case "wednesday":
            case "weds":
            case "w":
                day = 3;
                break;
            case "thursday":
            case "thurs":
            case "th":
                day = 4;
                break;
            case "friday":
            case "fri":
            case "f":
                day = 5;
                break;
            case "saturday":
            case "sat":
            case "sa":
                day = 6;
                break;
            case "sunday":
            case "sun":
            case "s":
                day = 7;
                break;  
            default:
                day = -1;
                break;
        }
        if(day < 0){
            System.out.println("That is not a day I know.");
        } else{
            System.out.println("The day of the week number is: " + day);
        }
    }
}

