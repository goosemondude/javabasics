/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.javabasics.controlflow;

import java.util.Scanner;
import java.util.Random;
/**
 *
 * @author apprentice
 */
public class LoopDeLoops {
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        Random randomGenerator = new Random();
        
//        while(true){
//            System.out.println("THIS IS AN INFINITE LOOP!!!!");
//        }
        //The conditional statement in your while loop SHOULD be able to change
        //Therefore, it needs to utilize variables - aka something that changes
        // OTHERWISE it will always be either true or false, and either not run
        // or run .... forever. and ever. and ever. and ever some more.
        
        int x = 50;
        while(x > 0) {
            System.out.println(x);
            int randomNumberBetweenZeroAndNine = randomGenerator.nextInt(10);
            x = x - randomNumberBetweenZeroAndNine; 
        }
        
        do{
            System.out.println("This will print how many times???");//once
        }while(false);
        
       
        
//        while(false){
//            System.out.println("This NEVER prints out.");
//        }
    }
}
