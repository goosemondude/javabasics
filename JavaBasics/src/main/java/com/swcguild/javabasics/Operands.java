/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.javabasics;

/**
 *
 * @author apprentice
 */
public class Operands {
    public static void main(String[] args) {
        int numOfLlamas;
        numOfLlamas = 5;
        
        int threeTimesMyLlamas = numOfLlamas * 3;
        
        //But then a llama died
        numOfLlamas = numOfLlamas - 1;
        
        System.out.println("The number of my llamas is: " + numOfLlamas);
        
        int a = 1 + 2;
        int b = 1 - 5;
        int c = 2 / 2;
        int d = 9 * 100;
        int e = 9 % 2;
 
        // Both of the below add ONE to the variable
        // but a++ adds one AFTER it returns the value, so will return 3, but store 4 in a
        // ++b adds one BEFORE it returns the value, so will return -3 and store -3 in b
        System.out.println(a++);
        System.out.println(++b);
        
        // These all accomplish the same goal of adding ONE to a and storing it in that memory location
        a = a + 1;
        a++;
        a += 1;
        
        System.out.println(c);
        c += d;
        System.out.println(c);
        
        e = a;
        // You can use as many operators and operands as you like
        // Just keep in mind the PEMDAS order of operation
        int result = (a + b) + (c + d) - e;


    }
}
