/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.contactlist.controller;

import com.swcguild.contactlist.dao.ContactListDao;
import com.swcguild.contactlist.model.Contact;
import java.util.List;
import java.util.Random;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class HomeControllerNoAjax {

    // Reference to the Dao
    private ContactListDao dao;

    @Inject // This replaces the controller bean & constructor
    // that we've done in the application context xml from before
    public HomeControllerNoAjax(ContactListDao dao) {
        this.dao = dao; // plug in the parameer into our 
        // dao shaped hole reference & property 
    }

    @RequestMapping(value = "/displayContactListNoAjax", method = RequestMethod.GET)
    public String displayContactListNoAjax(Model model) {

        this.addABunchaContacts();
        // Get a list of your contacts
        List<Contact> contacts = dao.getAllContacts();

        // Then update the model so that SpringMVC can passit
        // along to the view that is going to display it!
        model.addAttribute("contactList", contacts);

        // return the name of the view
        return "noajax/otherhome"; // turns into jsp/noajax/home.jsp via View Resolver
    }

    @RequestMapping(value = "/deleteContactNoAjax", method = RequestMethod.GET)
    public String deleteContactNoAjax(HttpServletRequest request) {
        String contactIdInput = request.getParameter("contactId");

        if (contactIdInput != null && !contactIdInput.isEmpty()) {
            try {
                int contactId = Integer.parseInt(contactIdInput);
                dao.removeContact(contactId);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

        }

        return "redirect:displayContactListNoAjax";
    }

    @RequestMapping(value = "/displayEditContactFormNoAjax", method = RequestMethod.GET)
    public String editContactNoAjax(HttpServletRequest request, Model model) {
        String contactIdInput = request.getParameter("contactId");

        if (contactIdInput != null && !contactIdInput.isEmpty()) {
            try {
                int contactId = Integer.parseInt(contactIdInput);
                Contact contact = dao.getContactById(contactId);
                model.addAttribute("contact", contact);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                return "redirect:displayContactListNoAjax";
            }

        }
        return "noajax/otheredit";

    }

    @RequestMapping(value = "editContactNoAjax", method = RequestMethod.POST)
//    public String editContactNoAjax(@ModelAttribute("contact") Contact xenophormagic){
//    //public String editContactNoAjax(@ModelAttribute("contact")Contact contact){
//        dao.updateContact(xenophormagic);
//        return "redirect:displayContactListNoAjax";
//    }
    public String editContactNoAjax(@Valid @ModelAttribute("contact") Contact xenophormagic,
            BindingResult result) {
        //public String editContactNoAjax(@ModelAttribute("contact")Contact contact){

        if (result.hasErrors()) {
            return "noajax/otheredit";
        }

        dao.updateContact(xenophormagic);
        return "redirect:displayContactListNoAjax";
    }

    @RequestMapping(value = "/newContactFormNoAjax", method = RequestMethod.GET)
    public String addContactFormNoAjax() {
        return "noajax/otheradd";
    }

    @RequestMapping(value = "/addNewContactNoAjax", method = RequestMethod.POST)
    public String processNewContactFormRequestNoAjax(HttpServletRequest request, Model model) {

        String firstNameInput = request.getParameter("firstName");
        String lastNameInput = request.getParameter("lastName");
        String companyInput = request.getParameter("company");
        String emailInput = request.getParameter("email");
        String phoneInput = request.getParameter("phone");

        Contact contact = new Contact();
        contact.setFirstName(firstNameInput);
        contact.setLastName(lastNameInput);
        contact.setCompany(companyInput);
        contact.setEmail(emailInput);
        contact.setPhone(phoneInput);

        dao.addContact(contact);

        model.addAttribute("contactList", dao.getAllContacts());
        return "noajax/otherhome";
    }

    private void addABunchaContacts() {
        Contact[] contacts = {
            new Contact(0, "Doctor", "Who", "Tardis", "555-BLU-WHOO", "thedoctah@gallifrey.com"),
            new Contact(1, "Zach", "Galifiniakis", "TheMovies", "123-ZAC-HGAL", "whoiszach@galifiniakis.com"),
            new Contact(2, "Jacob", "Naber", "AirForce", "828-388-1174", "jnaber25@hotmail.com"),
            new Contact(3, "Dick", "Winters", "502PIR", "456-CAL-LDIC", "thebrothers@dick.com"),
            new Contact(4, "Alison", "Richey", "Wick's", "502-804-8984", "alirichey1095@gmail.com"),
            new Contact(5, "James", "McJamerson", "TheJamesCo", "987-CAL-JAMS", "thegreatjames@jamesco.com"),
            new Contact(6, "Mr.", "Face", "FaceFactory", "777-CAL-URFC", "thefaceguy@facefriends.com"),
            new Contact(7, "Murphy", "G", "BeCool", "555-ARF-RARF", "givememore@dogfood.com"),
            new Contact(8, "Fritz", "theFritz", "Chillin", "502-475-9878", "knowckyourtea@over.com"),
            new Contact(9, "Pablo", "Robbins", "Codin'", "102-COD-ECOD", "gooserjg@hotmail.com")
        };

        Random r = new Random();
        dao.addContact(contacts[r.nextInt(contacts.length)]);
//        for (Contact c : contacts){
//            dao.addContact(c);
//        }

    }

}
