/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.contactlist.controller;

import com.swcguild.contactlist.dao.ContactListDao;
import com.swcguild.contactlist.model.Contact;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author apprentice
 */
@Controller
public class HomeController {

    private ContactListDao dao;

    @Inject
    public HomeController(ContactListDao dao) {
        this.dao = dao;

    }

    @ResponseBody
    @RequestMapping(value = "/contact/{id}", method = RequestMethod.GET)
    public Contact getContact(@PathVariable("id") int id) {

        return dao.getContactById(id);

    }

    @ResponseBody
    @RequestMapping(value = "/contacts", method = RequestMethod.GET)
    public List<Contact> getAllContacts() {

        return dao.getAllContacts();
    }

    // UPDATE
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/contact/{id}", method = RequestMethod.PUT)
    public void updateContact(@PathVariable("id") int id, @RequestBody Contact contact) {

        contact.setContactId(id);
        dao.updateContact(contact);

    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/contact/{id}", method = RequestMethod.DELETE)
    public void deleteContact(@PathVariable("id") int id) {

        dao.removeContact(id);
    }

    @ResponseBody
    @RequestMapping(value = "/contact", method = RequestMethod.POST)
    public Contact createContact(@Valid @RequestBody Contact contact) {

        return dao.addContact(contact);

    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value="/bunchaContacts", method = RequestMethod.GET)
    public void addABunchaContacts() {
        Contact[] contacts = {
            new Contact(0, "Doctor", "Who", "Tardis", "555-BLU-WHOO", "thedoctah@gallifrey.com"),
            new Contact(1, "Zach", "Galifiniakis", "TheMovies", "123-ZAC-HGAL", "whoiszach@galifiniakis.com"),
            new Contact(2, "Jacob", "Naber", "AirForce", "828-388-1174", "jnaber25@hotmail.com"),
            new Contact(3, "Dick", "Winters", "502PIR", "456-CAL-LDIC", "thebrothers@dick.com"),
            new Contact(4, "Alison", "Richey", "Wick's", "502-804-8984", "alirichey1095@gmail.com"),
            new Contact(5, "James", "McJamerson", "TheJamesCo", "987-CAL-JAMS", "thegreatjames@jamesco.com"),
            new Contact(6, "Mr.", "Face", "FaceFactory", "777-CAL-URFC", "thefaceguy@facefriends.com"),
            new Contact(7, "Murphy", "G", "BeCool", "555-ARF-RARF", "givememore@dogfood.com"),
            new Contact(8, "Fritz", "theFritz", "Chillin", "502-475-9878", "knowckyourtea@over.com"),
            new Contact(9, "Pablo", "Robbins", "Codin'", "102-COD-ECOD", "gooserjg@hotmail.com")
        };

        for (Contact contact : contacts) {
            dao.addContact(contact);
        }
    }

//    public void setDao(ContactListDao dao){
//        this.dao = dao;
//    }
    // A Spring controller method should be public
    // return a string (aka the view name) have a good descriptive name
    // and then take in any parameters it needs (aka HttpServletRequest, Model, etc.
    // but if it doesn't need them, it doesn't have to!!
    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String displayHomePage() {
        return "home"; // I don't want to do anything except go home.
    }

}
