/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.contactlist.dao;

import com.swcguild.contactlist.model.Contact;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

/**
 *
 * @author apprentice
 */
public interface ContactListDao {
    
    // We need to define the basic crud funcionality here
    // and then someone else who knows more code, will implement it
    
    public Contact addContact(Contact contact);
    
    // READ
    public Contact getContactById(int contactId);
    public List<Contact> getAllContacts();
    
    public List<Contact> searchContacts(Map<SearchTerm, String> criteria);
    public List<Contact> searchContacts(Predicate<Contact> filter);
    
    // UPDATE
    public void updateContact(Contact contact);
    
    // DELETE
    public Contact removeContact(int contactId);
}
