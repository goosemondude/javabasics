/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.contactlist.dao;

import com.swcguild.contactlist.model.Contact;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class ContactListDaoInMemImpl implements ContactListDao {

    private ArrayList<Contact> contacts = new ArrayList<>();
    private static int contactIdCounter = 0;

    @Override
    public Contact addContact(Contact contact) {
        // Make sure that new contacts have an id!
        contact.setContactId(contactIdCounter);
        contactIdCounter++; // Then make sure that you increment
        // So that the NEXT contact you add will have a new number!
        // Add the contact to the list!

//        if(null == contacts.get(contactIdCounter)){
//            
//        }
        contacts.add(contact);
        return contact;
    }

    @Override
    public Contact getContactById(int contactId) {

//        for (Contact contact : contacts){
//            
//            if(contact.getContactId() == contactId)
//                return contact;
//        }
//        
//        return null;
        Optional<Contact> maybeDuck = contacts.stream()
                .filter(contact -> contact.getContactId() == contactId)
                .findFirst();

        if (maybeDuck.isPresent()) {
            return maybeDuck.get();
        }
        return null;
    }

    @Override
    public List<Contact> getAllContacts() {
        return contacts;
    }

    @Override
    public List<Contact> searchContacts(Map<SearchTerm, String> criteria) {
        
        String firstNameCriteria = criteria.get(SearchTerm.FIRST_NAME);
        String lastNameCriteria = criteria.get(SearchTerm.LAST_NAME);
        String companyCriteria = criteria.get(SearchTerm.COMPANY);
        String emailCriteria = criteria.get(SearchTerm.EMAIL);
        String phoneCriteria = criteria.get(SearchTerm.PHONE);
        
        Predicate<Contact> firstNameMatches;
        Predicate<Contact> lastNameMatches;
        Predicate<Contact> companyMatches;
        Predicate<Contact> emailMatches;
        Predicate<Contact> phoneMatches;
        
        Predicate<Contact> allPass = (contact) -> {return true;};
        
        firstNameMatches = (firstNameCriteria == null || firstNameCriteria.isEmpty())?
                allPass : contact -> contact.getFirstName().toLowerCase().contains(firstNameCriteria.toLowerCase());
        lastNameMatches = (lastNameCriteria == null || lastNameCriteria.isEmpty())?
                allPass : contact -> contact.getLastName().toLowerCase().contains(lastNameCriteria.toLowerCase());
        companyMatches = (companyCriteria == null || companyCriteria.isEmpty())?
                allPass : contact -> contact.getCompany().toLowerCase().contains(companyCriteria.toLowerCase());
        emailMatches = (emailCriteria == null || emailCriteria.isEmpty())?
                allPass : contact -> contact.getCompany().toLowerCase().contains(emailCriteria.toLowerCase());
        phoneMatches = (phoneCriteria == null || phoneCriteria.isEmpty())?
                allPass : contact -> contact.getCompany().contains(phoneCriteria);
        
        List<Contact> results=
        contacts.stream()
                .filter(firstNameMatches)
                .filter(lastNameMatches)
                .filter(companyMatches)
                .filter(emailMatches)
                .filter(phoneMatches)
                .collect(Collectors.toList());
        
        return results;
        
        
    }

    @Override
    public List<Contact> searchContacts(Predicate<Contact> filter) {
        
        return contacts.stream()
                .filter(filter)
                .collect(Collectors.toList());

    }

    @Override
    public void updateContact(Contact contact) {
        Contact x = this.getContactById(contact.getContactId());
        if(x != null){
            contacts.remove(x);
            contacts.add(contact);
        }
    }

    @Override
    public Contact removeContact(int contactId) {
        
        Contact x = this.getContactById(contactId);
        if(x != null) contacts.remove(x);
        return x;
        
    }

}
