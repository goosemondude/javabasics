/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.contactlist.dao;

import com.swcguild.contactlist.model.Contact;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javafx.util.Pair;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class ContactListDaoJdbcImpl implements ContactListDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String INSERT_CONTACT
            = "INSERT INTO Contacts"
            + "(`first_name`, `last_name`, `phone`, `email`)"
            + "VALUES (?,?,?,?)";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Contact addContact(Contact contact) {
        jdbcTemplate.update(INSERT_CONTACT,
                contact.getFirstName(),
                contact.getLastName(),
                contact.getPhone(),
                contact.getEmail());
        int id = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        // TODO: Insert the company id and or company here
        contact.setContactId(id);
        this.updateContactCompany(contact);
        return contact;
    }

    private static final String SELECT_CONTACT_BY_ID
            = " SELECT * FROM Contacts\n "
            + "	LEFT JOIN Companies\n "
            + " ON Contacts.company_id = Companies.company_id\n "
            + " WHERE contact_id = ?";

    @Override
    public Contact getContactById(int contactId) {
        try {
            return jdbcTemplate.queryForObject(SELECT_CONTACT_BY_ID, new ContactMapper(), contactId);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }

    }

    private static final String SELECT_ALL_CONTACTS
            = " SELECT * FROM Contacts\n "
            + " LEFT JOIN Companies\n "
            + " ON Contacts.company_id = Companies.company_id ";

    @Override
    public List<Contact> getAllContacts() {
        return jdbcTemplate.query(SELECT_ALL_CONTACTS, new ContactMapper());
    }

    @Override
    public List<Contact> searchContacts(Map<SearchTerm, String> criteria) {
        String firstNameCriteria = criteria.get(SearchTerm.FIRST_NAME);
        String lastNameCriteria = criteria.get(SearchTerm.LAST_NAME);
        String companyCriteria = criteria.get(SearchTerm.COMPANY);
        String emailCriteria = criteria.get(SearchTerm.EMAIL);
        String phoneCriteria = criteria.get(SearchTerm.PHONE);

        Predicate<Contact> firstNameMatches;
        Predicate<Contact> lastNameMatches;
        Predicate<Contact> companyMatches;
        Predicate<Contact> emailMatches;
        Predicate<Contact> phoneMatches;

        Predicate<Contact> allPass = (contact) -> {
            return true;
        };

        firstNameMatches = (firstNameCriteria == null || firstNameCriteria.isEmpty())
                ? allPass : contact -> contact.getFirstName().toLowerCase().contains(firstNameCriteria.toLowerCase());
        lastNameMatches = (lastNameCriteria == null || lastNameCriteria.isEmpty())
                ? allPass : contact -> contact.getLastName().toLowerCase().contains(lastNameCriteria.toLowerCase());
        companyMatches = (companyCriteria == null || companyCriteria.isEmpty())
                ? allPass : contact -> contact.getCompany().toLowerCase().contains(companyCriteria.toLowerCase());
        emailMatches = (emailCriteria == null || emailCriteria.isEmpty())
                ? allPass : contact -> contact.getCompany().toLowerCase().contains(emailCriteria.toLowerCase());
        phoneMatches = (phoneCriteria == null || phoneCriteria.isEmpty())
                ? allPass : contact -> contact.getCompany().contains(phoneCriteria);

        List<Contact> results
                = this.getAllContacts().stream()
                .filter(firstNameMatches)
                .filter(lastNameMatches)
                .filter(companyMatches)
                .filter(emailMatches)
                .filter(phoneMatches)
                .collect(Collectors.toList());

        return results;
    }

    @Override
    public List<Contact> searchContacts(Predicate<Contact> filter) {
        return getAllContacts().stream()
                .filter(filter)
                .collect(Collectors.toList());

    }

    private static final String UPDATE_CONTACT_BY_ID
            = "UPDATE Contacts SET first_name = ?, last_name = ?, phone = ?, email = ?"
            + "WHERE contact_id = ?";

    @Override
    public void updateContact(Contact contact) {
        jdbcTemplate.update(UPDATE_CONTACT_BY_ID,
                contact.getFirstName(),
                contact.getLastName(),
                contact.getPhone(),
                contact.getEmail(),
                contact.getContactId());
        
        this.updateContactCompany(contact);
    }

    private static final String DELETE_CONTACT_BY_ID
            = "DELETE FROM Contacts WHERE contact_id = ?";

    @Override
    public Contact removeContact(int contactId) {
        Contact removedContact = this.getContactById(contactId);
        jdbcTemplate.update(DELETE_CONTACT_BY_ID, contactId);
        return removedContact;
    }

    private static final class ContactMapper implements RowMapper<Contact> {

        @Override
        public Contact mapRow(ResultSet rs, int i) throws SQLException {
            Contact newContact = new Contact();

            String firstName = rs.getString("first_name");
            newContact.setFirstName(firstName);
            newContact.setEmail(rs.getString("email"));
            newContact.setLastName(rs.getString("last_name"));
            newContact.setCompany(rs.getString("company_name"));
            newContact.setPhone(rs.getString("phone"));
            newContact.setContactId(rs.getInt("contact_id"));

            return newContact;
        }
    }

    private static final String SELECT_ALL_COMPANIES = "SELECT * FROM Companies";

    private static final String INSERT_COMPANY = "INSERT INTO Companies\n"
            + "	(company_name)\n"
            + "VALUES (?)";

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    private int addCompany(String companyName) {

        jdbcTemplate.update(INSERT_COMPANY, companyName);
        return jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

    }

    private HashMap<Integer, String> getAllCompanies() {
        HashMap<Integer, String> companiesById = new HashMap<>();

        List<Pair<Integer, String>> companies
                = jdbcTemplate.query(SELECT_ALL_COMPANIES, new CompanyMapper());

        for (int i = 0; i < companies.size(); i++) {
            Pair<Integer, String> company = companies.get(i);
            companiesById.put(company.getKey(), company.getValue());
        }

        return companiesById;

    }

    private static final String UPDATE_CONTACT_COMPANY_BY_ID
            = "UPDATE Contacts SET company_id = ? WHERE contact_id = ?";

    private void updateContactCompany(Contact contactToUpdate) {
        HashMap<Integer, String> companies = this.getAllCompanies();
        String contactsCompany = contactToUpdate.getCompany();
        int companyID = -1;
        if (companies.containsValue(contactsCompany)) {
            
            for (Entry<Integer, String> companyPair : companies.entrySet()){
                if (companyPair.getValue().equals(contactToUpdate.getCompany())){
                    companyID = companyPair.getKey();
                    break;
                }
            }

        } else {
            companyID = this.addCompany(contactsCompany);
        }
            jdbcTemplate.update(UPDATE_CONTACT_COMPANY_BY_ID,
                    companyID,
                    contactToUpdate.getContactId());
    }

    private static final class CompanyMapper implements RowMapper<Pair<Integer, String>> {
// company_id, company_name

        @Override
        public Pair<Integer, String> mapRow(ResultSet rs, int i) throws SQLException {

            Integer companyId = rs.getInt("company_id");
            String companyName = rs.getString("company_name");

            Pair<Integer, String> keyValuePair = new Pair(companyId, companyName);

            return keyValuePair;

        }
    }
}
