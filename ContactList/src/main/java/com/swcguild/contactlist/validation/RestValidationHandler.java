/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.contactlist.validation;

import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author apprentice
 */
@ControllerAdvice

public class RestValidationHandler {
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ValidationErrorContainer processValidationErrors(
            MethodArgumentNotValidException e) {
        
        ValidationErrorContainer errors = new ValidationErrorContainer();
        
        BindingResult result = e.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        
        fieldErrors.stream().forEach(fieldError -> errors.addValidationError(fieldError.getField(), fieldError.getDefaultMessage()));

//        for(FieldError error : fieldErrors){
//            errors.addValidationError(error.getField(), error.getDefaultMessage());
//        }
        return errors;
    }

}
