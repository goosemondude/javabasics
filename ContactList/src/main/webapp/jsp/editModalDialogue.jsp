<%-- 
    Document   : editModalDialogue
    Created on : Jul 21, 2016, 11:13:17 AM
    Author     : apprentice
--%>
<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby
             ="editModalLabel" aria-hidden="true">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h2 id="contact-id">ContactId Godes</h2>

                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="edit-first-name" class="col-md-4 control-label">
                                    First Name:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-first-name" placeholder="First Name" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-last-name" class="col-md-4 control-label">
                                    Last Name:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-last-name" placeholder="Last Name" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-company" class="col-md-4 control-label">
                                    Company:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-company" placeholder="Company" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-phone" class="col-md-4 control-label">
                                    Phone:
                                </label>
                                <div class="col-md-8">
                                    <input type="tel" class="form-control" id="edit-phone" placeholder="Phone" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-email" class="col-md-4 control-label">
                                    Email:
                                </label>
                                <div class="col-md-8">
                                    <input type="email" class="form-control" id="edit-email" placeholder="Email" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8">
                                    <button id="edit-button" class="btn btn-default" data-dismiss="modal">Edit Contact</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" id="edit-contact-id"/>
                                </div>
                            </div>
                        </form>


                    </div>

                </div>

            </div>
        </div>