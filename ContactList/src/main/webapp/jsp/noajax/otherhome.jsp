<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Company Contacts</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Company Contacts</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/home">AjaxHome</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/search">Search</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/stats">Stats</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/displayContactListNoAjax">NoAjaxHome</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/newContactFormNoAjax">Add New Contact</a></li>
                </ul>    
            </div>
            <h2>Home Page No Ajax</h2>


            <c:forEach var="contact" items="${contactList}">
                <div
                    <c:if test="${contact.company =='Tardis'}">
                        style="color: #8962ed"
                    </c:if>
                    <c:if test="${contact.company =='TheJamesCo'}">
                        style="color: #2f79ff"
                    </c:if>
                    <c:if test="${contact.company =='BeCool'}">
                        style="color: #c9b03f"
                    </c:if>
                    <c:if test="${contact.company =='Chillin'}">
                        style="color: #dc315e"
                    </c:if>
                    <c:if test="${contact.company =='FaceFactory'}">
                        style="color: #4edb4b"
                    </c:if>
                    <c:if test="${contact.company =='AirForce'}">
                        style="color: orange"
                    </c:if>
                    <c:if test="${contact.company =='TheMovies'}">
                        style="color: red"
                    </c:if>
                    <c:if test="${contact.company =='502PIR'}">
                        style="color: rosybrown"
                    </c:if>
                    <c:if test='${contact.company =="Wick\'s"}'>
                        style='color: aquamarine'
                    </c:if>
                    <c:if test='${contact.company =="Codin\'"}'>
                        style='color: #924f2b'
                    </c:if>
                    >

                    <!-- WE are going to build a custom URL for deleting a contact-->
                    <!-- so the place (or url) it's going to go to is deleteContact NoAjax
                        and giving that url a variable name of deleteContact_url -->
                    <s:url value="deleteContactNoAjax" var="deleteContact_url">
                        <!-- we want that link to also transmit a parameter - like any param in a form, it has a name, and it has a 
                        value. In this case, to delete a url, we want to send over a prameter of a contactId and
                        hae it match the contact id of the contact we are iterating over...! -->
                        <s:param name="contactId" value="${contact.contactId}" />
                    </s:url>

                    <!-- do it again, for an edit link! -->
                    <s:url value="displayEditContactFormNoAjax" var="editContact_url">
                        <s:param name="contactId" value="${contact.contactId}" />
                    </s:url>

                    <b>Name: </b>${contact.firstName} ${contact.lastName} <br/>
                    <b>Company: </b>${contact.company}<br />
                    <b>Phone: </b>${contact.phone} <br />
                    <b>Email: </b>${contact.email} <br />
                    <a href="${deleteContact_url}">Delete</a> | <a href="${editContact_url}">Edit</a>
                </div>
                <hr />
            </c:forEach>

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

