<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Company Contacts</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Company Contacts</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/home">AjaxHome</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/search">Search</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/stats">Stats</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/displayContactListNoAjax">NoAjaxHome</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/newContactFormNoAjax">Add New Contact</a></li>
                    
                </ul>    
            </div>
                    <div class="container">
                        <h1>New Contact Form:</h1>
                        <form action="addNewContactNoAjax" method ="POST">
                            <input type="text" id="addFirstName" name="firstName" placeholder="First Name"/>
                            <input type="text" id="addLastName" name="lastName" placeholder="Last Name"/>
                            <input type="text" id="addCompany" name="company" placeholder="Company"/>
                            <input type="email" id="addEmail" name="email" placeholder="Email"/>
                            <input type="tel" id="addPhone" name="phone" placeholder="Phone"/>
                            <button class="btn btn-primary" type="submit" id="">Add New Contact</button>
                        </form>
                    </div>

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

