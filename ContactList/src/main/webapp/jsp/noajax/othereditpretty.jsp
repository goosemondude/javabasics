<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Company Contacts</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Edit Contact Form</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/home">AjaxHome</a></li>
                    <!--<li role="presentation"><a href="${pageContext.request.contextPath}/search">Search</a></li>-->
                    <!--<li role="presentation"><a href="${pageContext.request.contextPath}/stats">Stats</a></li>-->
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/displayContactListNoAjax">NoAjaxHome</a></li>
                </ul>    
            </div>
            <h2>Edit</h2>


            <div class="container">
                <div class="container">
                    <h1>Edit Contact Form:</h1>
                    <c:if test="${contact == null}">
                        <h1>I'm sorry. That contact #${param.contactId} does not exist.</h1>
                    </c:if>


                    <c:if test="${contact != null}">
                        <sf:form action="editContactNoAjax" method ="POST" modelAttribute="contact">
                            <sf:input  type="text" id="editFirstName" path="firstName" placeholder="First Name"/>
                            <sf:errors path="firstName"></sf:errors>
                            <sf:input type="text" id="editLastName" path="lastName" placeholder="Last Name"/>
                            <sf:errors path="lastName"></sf:errors>
                            <sf:input type="text" id="editCompany" path="company" placeholder="Company"/>
                            <sf:errors path="company"></sf:errors>
                            <sf:input type="email" id="editEmail" path="email" placeholder="Email"/>
                            <sf:errors path="email"></sf:errors>
                            <sf:input type="tel" id="editPhone" path="phone" placeholder="Phone"/>
                            <sf:errors path="phone"></sf:errors>
                            <sf:hidden path="contactId"/>
                            <button class="btn btn-primary" type="submit" id="">Edit New Contact</button>
                        </sf:form>
                    </c:if>
                </div>

            </div>
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>
