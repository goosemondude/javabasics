/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    loadContacts();
    $('#add-button').click(function (event) {
        event.preventDefault();
        addContact();

    });
    
    $('#search-button').click(function(event){
        event.preventDefault();
        searchContacts();
    });
    
    $('#edit-modal').on('show.bs.modal', function(event){
        var element = $(event.relatedTarget);
        var contactId = element.data('contact-id');
       populateEditModal(contactId); 
    });
    
    $('#edit-button').click(function(event){
        event.preventDefault();
        editContact();
    });

});

function searchContacts(){
    var searchFirstName = $('#search-first-name').val();
    var searchLastName = $('#search-last-name').val();
    var searchCompany = $('#search-company').val();
    var searchEmail = $('#search-email').val();
    var searchPhone = $('#search-phone').val();
 
    
    $.ajax({
        url: 'search/contacts',
        type: 'POST',
        headers:{
            'Accept': 'application/json',
            'Content-Type':'application/json'
        },
        'dataType':'json',
        data: JSON.stringify({
            firstName:  searchFirstName,
            lastName: searchLastName,
            company: searchCompany,
            phone: searchPhone,
            email: searchEmail
        })
        
    }).success(function(filteredList){
        clearContactTable();
        fillContactTable(filteredList);
    });
    
}

function editContact(){
    var contactId = $('#edit-contact-id').val();
    
    var cFirstName = $('#edit-first-name').val();
    var cLastName = $('#edit-last-name').val();
    var cCompany = $('#edit-company').val();
    var cEmail = $('#edit-email').val();
    var cPhone = $('#edit-phone').val();
    
    $.ajax({
        type:'PUT',
        url:'contact/' + contactId,
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json',
        data: JSON.stringify({
            firstName: cFirstName,
            lastName: cLastName,
            company: cCompany,
            phone: cPhone,
            email: cEmail
                    
        })
    }).success(function () {
        loadContacts();
        $('#edit-first-name').val('');
        $('#edit-last-name').val('');
        $('#edit-company').val('');
        $('#edit-phone').val('');
        $('#edit-email').val('');
        
    });
}

function populateEditModal(id){
    $.ajax({
        type:'GET',
        url: 'contact/' + id,
        headers: {
            'Accept': 'application/json'
        }
    }).success(function(contact){
        $('#edit-first-name').val(contact.firstName);
        $('#edit-last-name').val(contact.lastName);
        $('#edit-company').val(contact.company);
        $('#edit-phone').val(contact.phone);
        $('#edit-email').val(contact.email);
        
        $('#edit-contact-id').val(contact.contactId);
        $('#contact-id').text(contact.contactId);
    });
    
}


function addContact() {
//debugger;
    var cFirstName = $('#add-first-name').val();
    var cLastName = $('#add-last-name').val();
    var cCompany = $('#add-company').val();
    var cPhone = $('#add-phone').val();
    var cEmail = $('#add-email').val();

    $.ajax({
        type: 'POST',
        url: 'contact',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json',
        data: JSON.stringify({
            firstName: cFirstName,
            lastName: cLastName,
            company: cCompany,
            phone: cPhone,
            email: cEmail


        })

    }).success(function () {
        var errorDiv = $('#validationErrors');
        errorDiv.empty();
        errorDiv.hide();
        
        loadContacts();
        $('#add-first-name').val('');
        $('#add-last-name').val('');
        $('#add-company').val('');
        $('#add-phone').val('');
        $('#add-email').val('');
    }).error(function(data, status){
        var errorDiv = $('#validationErrors');
        errorDiv.empty();
        errorDiv.show();
        
        $.each(data.responseJSON.validationErrors, function(index, validationError){
           errorDiv.append(validationError.message);
           errorDiv.append($('<br>'));
           
        });
    });

}


function loadContacts() {
    clearContactTable();

    var cTable = $('#contentRows');

    $.ajax({
        type: 'GET',
        url: 'contacts'

    }).success(function (contactsData) {
        fillContactTable(contactsData);
    });



//        cTable.append($('<tr>')
//                .append($('<td>').text(contact.firstName + ' ' + contact.lastName))
//                .append($('<td>').text(contact.company))
//                .append($('<td>').text('Edit'))
//                .append($('<td>').text('Delete'))
//                );


}


function fillContactTable(contacts) {
    var cTable = $('#contentRows');

    $.each(contacts, function (index, contact) {
        var name = $('<td>');
        name.text(contact.firstName + ' ' + contact.lastName);

        var company = $('<td>');
        company.text(contact.company);

        var edit = $('<td>');
        var editLink = $('<a>');
        editLink.text('Edit');
        editLink.attr({
           'data-toggle':'modal',
           'data-target':'#edit-modal',
           'data-contact-id': contact.contactId
           
        });
        edit.append(editLink);

        var del = $('<td>');
        var delLink = $('<a>');
        delLink.text('Delete');
        delLink.attr({'onclick': 'deleteContact(' + contact.contactId + ')'});
        del.append(delLink);

        var contactRow = $('<tr>');

        contactRow.append(name);
        contactRow.append(company);
        contactRow.append(edit);
        contactRow.append(del);
        cTable.append(contactRow);
    });
}

function deleteContact(id) {
    $.ajax({
        type: 'DELETE',
        url: 'contact/' + id
    }).success(function () {
        loadContacts();
    });
}


function clearContactTable() {
    $('#contentRows').empty();
}


var testContactData = [
    {
        contactId: 1,
        firstName: "Xena",
        lastName: "Warrior Princess",
        company: "Olympus",
        email: "xena@olympus.io",
        phone: "555-XENA"
    },
    {
        contactId: 2,
        firstName: "Rick",
        lastName: "James B****",
        company: "All of them",
        email: "rick@james.com",
        phone: "777-RICK"
    },
    {
        contactId: 3,
        firstName: "Bobby",
        lastName: "McBobberson",
        company: "The Bobs",
        email: "bob@by.org",
        phone: "999-BOBS"
    }

];
//like a lambda