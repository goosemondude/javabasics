/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.contactlist.dao;

import com.swcguild.contactlist.model.Contact;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class ContactListDaoTest {

    private ContactListDao testDao;
    private Contact[] contacts = {
        new Contact(0, "Doctor", "Who", "Tardis", "555-BLU-WHOO", "thedoctah@gallifrey.com"),
        new Contact(1, "Zach", "Galifiniakis", "TheMovies", "123-ZAC-HGAL", "whoiszach@galifiniakis.com"),
        new Contact(2, "Jacob", "Naber", "AirForce", "828-388-1174", "jnaber25@hotmail.com"),
        new Contact(3, "Dick", "Winters", "502PIR", "456-CAL-LDIC", "thebrothers@dick.com"),
        new Contact(4, "Alison", "Richey", "Wick's", "502-804-8984", "alirichey1095@gmail.com"),
        new Contact(5, "James", "McJamerson", "TheJamesCo", "987-CAL-JAMS", "thegreatjames@jamesco.com"),
        new Contact(6, "Mr.", "Face", "FaceFactory", "777-CAL-URFC", "thefaceguy@facefriends.com"),
        new Contact(7, "Murphy", "G", "BeCool", "555-ARF-RARF", "givememore@dogfood.com"),
        new Contact(8, "Fritz", "theFritz", "Chillin", "502-475-9878", "knowckyourtea@over.com"),
        new Contact(9, "Pablo", "Robbins", "Codin'", "102-COD-ECOD", "gooserjg@hotmail.com")
    };

    private Contact[] duplicateContacts = {
        new Contact(0, "Doctor", "Who", "Tardis", "555-BLU-WHOO", "thedoctah@gallifrey.com"),
        new Contact(1, "Zach", "Galifiniakis", "TheMovies", "123-ZAC-HGAL", "whoiszach@galifiniakis.com"),
        new Contact(2, "Jacob", "Naber", "AirForce", "828-388-1174", "jnaber25@hotmail.com"),
        new Contact(3, "Dick", "Winters", "502PIR", "456-CAL-LDIC", "thebrothers@dick.com"),
        new Contact(4, "Alison", "Richey", "Wick's", "502-804-8984", "alirichey1095@gmail.com"),
        new Contact(5, "James", "McJamerson", "TheJamesCo", "987-CAL-JAMS", "thegreatjames@jamesco.com"),
        new Contact(6, "Mr.", "Face", "FaceFactory", "777-CAL-URFC", "thefaceguy@facefriends.com"),
        new Contact(7, "Murphy", "G", "BeCool", "555-ARF-RARF", "givememore@dogfood.com"),
        new Contact(8, "Fritz", "theFritz", "Chillin", "502-475-9878", "knowckyourtea@over.com"),
        new Contact(9, "Pablo", "Robbins", "Codin'", "102-COD-ECOD", "gooserjg@hotmail.com")
    };

    private Contact[] similarContacts = {
        new Contact(0, "HeyYo", "Who", "Tardis", "555-BLU-WHOO", "thedoctah@gallifrey.com"),
        new Contact(1, "Zach", "Jeruskis", "TheMovies", "123-ZAC-HGAL", "whoiszach@galifiniakis.com"),
        new Contact(2, "Jacob", "Naber", "EarthForce", "828-388-1174", "jnaber25@hotmail.com"),
        new Contact(3, "Dick", "Winters", "502PIR", "123-123-123", "thebrothers@dick.com"),
        new Contact(4, "Alison", "Richey", "Wick's", "502-804-8984", "agreatgal@agreatgal.com"),
        new Contact(5, "Ramond", "McJamerson", "TheJamesCo", "987-CAL-JAMS", "thegreatjames@jamesco.com"),
        new Contact(6, "Mr.", "Booty", "FaceFactory", "777-CAL-URFC", "thefaceguy@facefriends.com"),
        new Contact(7, "Murphy", "G", "thebests", "555-ARF-RARF", "givememore@dogfood.com"),
        new Contact(8, "Fritz", "theFritz", "Chillin", "313-313-1313", "knowckyourtea@over.com"),
        new Contact(9, "Pablo", "Robbins", "Codin'", "102-COD-ECOD", "sillyfellow@whositswhatits.com")
    };

    public ContactListDaoTest() {
    }

    @Before
    public void setUp() {
        // ContextFactory is storing things as objects but we don't want an object we want a dao so we can do dao things
        ApplicationContext ctxFactory
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        testDao = ctxFactory.getBean("dao", ContactListDaoInMemImpl.class);
        //testDao = (ContactListDao)ctxFactory.getBean("dao");

    }

    @After
    public void tearDown() {
    }
// TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}

    @Test
    public void testAddContact() {

        testDao.addContact(contacts[0]);
        Assert.assertNotNull("Added a contact, but all contacts list was null.", testDao.getAllContacts());
        Assert.assertEquals("Added a contact, chould have had a size 1, wasn't.",1, testDao.getAllContacts().size());
        Assert.assertEquals("Added, got it back, wasn't the same one.", contacts[0], testDao.getAllContacts().get(0));
    }

    @Test
    public void testAddContacts() {
            
        // First, add all of the contacts in the array
        for (Contact c : contacts){
            testDao.addContact(c);
        }
        //Then, ask the Dao for all of its contacts - the list returned SHOULD 
        Assert.assertNotNull("Added a contact, but all contacts list was null.", testDao.getAllContacts());
        // Then, check the SIZE of the returned list, it should match the size
        Assert.assertEquals("Added a contact, chould have had a size 10, wasn't.",10, testDao.getAllContacts().size());
        
        // Then, check the
        for (int i = 0; i < contacts.length; i++) {
            Assert.assertTrue("Returned list of contacts, doesn't contain expected.", testDao.getAllContacts().contains(contacts[i]));
        }
    }


//    public Contact addContact(Contact contact);
//    
//    // READ
//    public Contact getContactById(int contactId);
//    public List<Contact> getAllContacts();
//    
//    public List<Contact> searchContacts(Map<SearchTerm, String> criteria);
//    public List<Contact> searchContacts(Predicate<Contact> filter);
//    
//    // UPDATE
//    public void updateContact(Contact contact);
//    
//    // DELETE
//    public Contact removeContact(int contactId);
}
