/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.pizzalist;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "PizzaMenu", urlPatterns = {"/PizzaMenu"})
public class PizzaMenu extends HttpServlet {
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PizzaMenu</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PizzaMenu at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Pizza> pizzas = new ArrayList<>();
        Pizza pepperoni = new Pizza("Pepperoni Pizza", "A Delicious Pepperoni Pizza",  5.00);
        pizzas.add(pepperoni);
        pizzas.add(new Pizza("Spinach PIzza", "A slightly healthier pizza covered in spinach. But also cheese. Cause it's a pizza.", 15.00));
        pizzas.add(new Pizza("Fried Chicken PIzza", "A slightly unhealthier pizza covered in fried Chicken. It's a grease-mobster", 9.00));
        pizzas.add(new Pizza("Bologna PIzza", "Gravy and Bologna and biscuits. Cause it's your mom making breakfast", 12.00));
        pizzas.add(new Pizza("Burger Pizza", "10 McDonald's cheeseburgers 'deconstructed' for your eating pleasure.", 400.00));
        
        
        request.setAttribute("pizzaMenu", pizzas);
        request.setAttribute("numPizzas", pizzas.size());
        
        // let's send her on over
        RequestDispatcher forwarder = request.getRequestDispatcher("menu.jsp");
        forwarder.forward(request, response);
        
        // request.getRequestDispatcher("menu.jsp").forward(request, response);
                
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
