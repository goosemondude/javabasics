<%-- 
    Document   : menu
    Created on : Jul 8, 2016, 11:43:36 AM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Here are the delicious Pizzas available.</title>
    </head>
    <body>
        <h1>Delicious Pizza Menu</h1>
        <table>   
            <c:forEach items="${pizzaMenu}" var="pizza">
            <tr>
                <td>${pizza.name} : $ ${pizza.price}</td>
            <td>${pizza.desc}</td>
            </tr>
                
            </c:forEach>
        </table>
    </body>
</html>
