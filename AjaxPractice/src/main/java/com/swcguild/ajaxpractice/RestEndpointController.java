/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.ajaxpractice;

import java.util.Random;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */

@Controller
public class RestEndpointController {
    
    
    Random r = new Random();
    
    
    @RequestMapping(value="/rollDie", method=RequestMethod.GET)
    @ResponseBody
    public int rolld6(){
        
        return r.nextInt(6) + 1;
        
        
    }
    
    
    @RequestMapping(value="/rollDie/sides/{nsides}", method=RequestMethod.GET)
    @ResponseBody
    public int rollNSidedDie(@PathVariable("nsides")int n){
        
        
        return r.nextInt(n) + 1;
        
    }
    
    @RequestMapping(value="/testDouble", method=RequestMethod.GET)
    @ResponseBody
    public Double testObjReturn(){
        
        return new Double(9);
    }
    
    
}
