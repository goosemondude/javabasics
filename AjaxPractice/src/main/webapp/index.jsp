<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Spring MVC Application from Archetype</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/index.jsp">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/hello/sayhi">Hello Controller</a></li>
                </ul>    
            </div>
            <div class="container">
                <div id="ipgoeshere">Not Loaded Yet</div>
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script>
            $.ajax({
                url: "http://ip.jsontest.com/"
            }).success(function (data, textStatus, jqXHR) {
                // This is a NEWL"Y DEFINED cuntion that WILL BE CALLED
                // IF AND WHEN the ajax request above, comes back
                // with a SUCCESFUL response
                // Data, will come back, with a property called ip
                // you can reference that!
                // alert(data.ip);
                // But maybe, we want to do more than just alert
                // WE want to create or update a field on the page!
                $("#ipgoeshere").append(
                        "<div>" + data.ip + "</div>");







            });
        </script>


    </body>
</html>

